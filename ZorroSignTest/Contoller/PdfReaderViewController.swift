//
//  PdfReaderViewController.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import UIKit
import PDFKit

class PdfReaderViewController: UIViewController {

    @IBOutlet weak var draggableTextField: UITextFieldX!
    
    let sharedApiService   =   APIService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Add gesture to draggableTextField
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(userDragged))
        draggableTextField.addGestureRecognizer(gesture)
        draggableTextField.isUserInteractionEnabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        downloadPdfDocument()
    }
    
    @objc func userDragged(gesture: UIPanGestureRecognizer){
        let newLocation = gesture.location(in: self.view)
        self.draggableTextField.center = newLocation
    }

}

// APIs
extension PdfReaderViewController {
    
    func downloadPdfDocument() {
        
        sharedApiService.getDocument { (success) in
            
            // Add PDFView
            let pdfView = PDFView(frame: self.view.bounds)
            pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(pdfView)
            pdfView.autoScales = true
            
            if let pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last {
                pdfView.document = PDFDocument(url: pdfURL.appendingPathComponent("5 pages.pdf"))
            }
            
            // Send pdf viewer to back of the stack
            self.view.sendSubviewToBack(pdfView)
        }
       
    }
    
}
