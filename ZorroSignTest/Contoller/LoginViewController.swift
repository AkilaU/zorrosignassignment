//
//  ViewController.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextFieldX!
    @IBOutlet weak var passwordTextField: UITextFieldX!
    
    let sharedApiService   =   APIService.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
 
    }
    
    // Check for valid credentials
    func validCredentials(username:String, password:String) -> Bool {
        
        if username == "" && password == ""{
            usernameTextField.shake()
            passwordTextField.shake()
            return false
        }
        
        if username == "" {
            usernameTextField.shake()
            return false
        }
        
        if password == "" {
            passwordTextField.shake()
            return false
        }
        
        return true
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""

        if validCredentials(username : username, password: password) {
            let credentials = LoginCredentials(username: username, password: password)
            authenticate(credentials: credentials)
        }
    }
    
}

// APIs
extension LoginViewController {
    
    func authenticate(credentials: LoginCredentials) {
        sharedApiService.login(credentials: credentials, completion: { (user, success) in
            if success {
                print("Logged User: ",user?.UserName ?? "")
                print("Access token: ", AccessToken.shared.get())
                self.performSegue(withIdentifier: "loginToPdfReader", sender: nil)
            }
        })
    }
    
}
