//
//  Account.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation

class User: Codable {
    
    var UserId              :   String
    var UserName            :   String
    var IsActive            :   Bool
    
    init() {
        self.UserId         =   ""
        self.UserName       =   ""
        self.IsActive       =   false
    }

}
