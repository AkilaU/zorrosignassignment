//
//  LoginCredentials.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation

class LoginCredentials: Codable {
    
    var username                :   String
    var password                :   String
    
    init(username       :   String,
         password       :   String)
    {
        self.username   =   username
        self.password   =   password
    }
}
