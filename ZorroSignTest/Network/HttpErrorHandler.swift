//
//  HttpErrorHandler.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation
import SCLAlertView

struct HTTPErrorHandler {
    
    static let shared = HTTPErrorHandler()
    
    func handle(errorCode:Int, errorMsg: String? = "") {
        
        switch errorCode {
        case HTTPErrorCode.NO_INTERNET_ERROR.rawValue:
            showAlert(subTitle: HTTPErrorMsg.NO_INTERNET_ERROR.rawValue)
            
        case HTTPErrorCode.RESPONSE_ERROR.rawValue:
            showAlert(subTitle: HTTPErrorMsg.CONNECTION_ERROR.rawValue)
            
        case HTTPErrorCode.TIME_OUT_ERROR.rawValue:
            showAlert(subTitle: HTTPErrorMsg.TIME_OUT_ERROR.rawValue)
            
        case HTTPErrorCode.JSON_ERROR.rawValue:
            showAlert(subTitle: HTTPErrorMsg.JSON_ERROR.rawValue)
            
        case HTTPErrorCode.CUSTOM_ERROR.rawValue:
            showAlert(subTitle: errorMsg ?? HTTPErrorMsg.CONNECTION_ERROR.rawValue)
            
        default:
            showAlert(subTitle: HTTPErrorMsg.CONNECTION_ERROR.rawValue)
        }
        
    }
    
    func showAlert(subTitle: String) {
        SCLAlertView().showError("Error", subTitle: subTitle)
    }
    
}
