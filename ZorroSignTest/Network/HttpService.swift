//
//  HttpService.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct HTTPService {
    
    static let shared   =   HTTPService()
    let manager         =   Alamofire.SessionManager.default
    
    
    let TIME_OUT_INTERVAL: TimeInterval = 2
    
    
    // POST Request
    func postRequest(url:String, headers:[String: String]?, parameters:[String: Any]?, completion: @escaping (JSON, Int) -> ()) {
        
        print("URL:: ",url)
        
        if !Connectivity.isConnectedToInternet {
          return completion([], HTTPErrorCode.NO_INTERNET_ERROR.rawValue)
        }
        
        manager.session.configuration.timeoutIntervalForRequest = TIME_OUT_INTERVAL
        
        manager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print(response.request as Any)
            let compiledResponse = self.handleResponse(response: response)
            return completion(compiledResponse.reponse, compiledResponse.code)
        }
      
    }
    
    
    // Download Content
    func download(url:String, headers:[String: String]?, parameters:[String: Any]?, completion: @escaping (Int) -> ()) {
        
        print("URL:: ",url)
            
        if !Connectivity.isConnectedToInternet {
          return completion(HTTPErrorCode.NO_INTERNET_ERROR.rawValue)
        }
        
        guard let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return
        }
                
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        
        //destination.appendingPathComponent("zorrosign.pdf")

        Alamofire.download(
            urlString,
            method: .get,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers,
            to: destination).downloadProgress(closure: { (progress) in
                //progress
            }).response(completionHandler: { (downloadResponse) in

                print("Response:: ",downloadResponse)

                if let statusCode = downloadResponse.response?.statusCode {
                    return completion(statusCode)
                }else{
                    return completion(HTTPErrorCode.RESPONSE_ERROR.rawValue)
                }
        })
      
    }
    
    
    // Handle the response
    private func handleResponse(response: DataResponse<Any>) -> (reponse:JSON, code:Int) {
        
        print("StatusCode:: ",response.response?.statusCode ?? 0)
        print("Response:: ",response.data ?? [])
        
        var completion: (JSON, Int) = ([], 0)
         
        switch (response.result) {
        case .success:
            
            if let responseData = response.data , let statusCode = response.response?.statusCode, let jsonData = try? JSON(data: responseData) {
               completion = (jsonData, statusCode)
            } else {
                completion = ([], HTTPErrorCode.JSON_ERROR.rawValue)
            }
            
        case .failure(let error):
            
            if error._code == NSURLErrorTimedOut {
                completion = ([], HTTPErrorCode.TIME_OUT_ERROR.rawValue)
            } else {
                completion = ([], HTTPErrorCode.RESPONSE_ERROR.rawValue)
            }
            
        }
       
        return completion;
    }
}
