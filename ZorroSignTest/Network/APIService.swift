//
//  APIServiceLogin.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation

import SCLAlertView
import SwiftyJSON
import SwiftKeychainWrapper

struct APIService {
    
    static let shared           =   APIService()
    let sharedHTTPService       =   HTTPService.shared
    let sharedHTTPErrorHandler  =   HTTPErrorHandler.shared
    let alertAppearance         =   SCLAlertView.SCLAppearance(showCloseButton : false)
    
    // Authenticate User
    func login(credentials: LoginCredentials, completion: @escaping (_ user: User?, _ success: Bool) -> ()) {
        
        let headers         =   [
            "Accept"        : "application/json",
            "Content-Type"  : "application/json"
        ]
        
        let urlString       =  WebService.BASE_URL_USER.rawValue + WebService.LOGIN_URL.rawValue
        
        let parameters      =   [
            "UserName"      :   credentials.username,
            "Password"      :   credentials.password,
            "ClientId"      :   "123456",
            "ClientSecret"  :   "abcdef",
            "DoNotSendActivationMail" : false
            ] as [String : Any]
        
        let alert       =   SCLAlertView(appearance: alertAppearance).showWait("Authenticating", subTitle: "Please Wait...")
        
        sharedHTTPService.postRequest(url: urlString, headers: headers, parameters: parameters) { (jsonData, statusCode) in
      
            alert.close()
      
            if statusCode == 200 {
                print(jsonData)
                if jsonData["StatusCode"].intValue == 1000 {
                    let user = self.getUser(jsonData: jsonData) // get user object
                    self.saveToken(jsonData: jsonData) // save login access token
                    completion(user, true)
                }else {
                    let errorMsg = jsonData["Message"].stringValue
                    self.sharedHTTPErrorHandler.handle(errorCode: HTTPErrorCode.CUSTOM_ERROR.rawValue, errorMsg: errorMsg)
                }
            }else{
                self.sharedHTTPErrorHandler.handle(errorCode: statusCode)
                completion(User(),false)
            }
          
        }
      
    }
    
    
    // Get pdf document
    func getDocument(completion: @escaping (_ success: Bool) -> ()) {
        
        print("Token: ",AccessToken.shared.get())
        
        let headers         =   [
            "Authorization" : "Bearer \(AccessToken.shared.get())",
            "Content-Type"  : "application/X-Access-Token"
        ]
        
        let urlString       =  WebService.BASE_URL_WORKFLOW.rawValue + WebService.DOC_DOWNLOAD_URL.rawValue + "/13850/document?objectId=workspace://SpacesStore/1401c5c7-5b65-4978-b1d4-3e8650616212;1.0"
        
        let alert       =   SCLAlertView(appearance: alertAppearance).showWait("Downloading", subTitle: "Please Wait...")
        
        sharedHTTPService.download(url: urlString, headers: headers, parameters: nil) { (statusCode) in
            alert.close()
            
            if statusCode == 200 {
                completion(true)
            }else{
                completion(false)
            }
            
        }
      
    }
    
    
    // Get the user details
    private func getUser(jsonData: JSON)-> User? {
        
        if let data = try? jsonData["Data"].rawData(), let user = try? JSONDecoder().decode(User.self, from: data) {
            return user
        }
        
        return nil
    }
    
    
    // Save access token securely
    private func saveToken(jsonData: JSON) {
        AccessToken.shared.save(token: jsonData["Data"]["ServiceToken"].stringValue)
    }
    
    
}
