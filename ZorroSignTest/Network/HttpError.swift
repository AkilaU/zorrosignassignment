//
//  HttpError.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation

//Error Codes
enum HTTPErrorCode : Int {
    case NO_INTERNET_ERROR  =   -1001
    case RESPONSE_ERROR     =   -1002
    case TIME_OUT_ERROR     =   -1003
    case JSON_ERROR         =   -1004
    case CUSTOM_ERROR       =   -1005
}

//Error Messages
enum HTTPErrorMsg : String {
    case NO_INTERNET_ERROR  =   "No Internet Connection. Please Try Again..."
    case CONNECTION_ERROR   =   "Connection Failure. Please Try Again..."
    case TIME_OUT_ERROR     =   "Request Timed Out. Please Try Again..."
    case JSON_ERROR         =   "Json Failure. Please Try Again..."
}
