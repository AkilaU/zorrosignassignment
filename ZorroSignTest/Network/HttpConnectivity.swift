//
//  HttpConnectivity.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
