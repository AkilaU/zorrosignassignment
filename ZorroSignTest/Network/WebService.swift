//
//  WebService.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation

enum WebService : String {
    
    case BASE_URL_USER      =   "https://zswebuserqa.entrusttitle.net/api/"
    case BASE_URL_WORKFLOW  =   "https://zswebworkflowqa.entrusttitle.net/api/"
    
    case LOGIN_URL          =   "Account/Login"
    case DOC_DOWNLOAD_URL   =   "v1/process"
    
}

