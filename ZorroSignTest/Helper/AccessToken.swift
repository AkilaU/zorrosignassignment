//
//  AccessToken.swift
//  ZorroSignTest
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

struct AccessToken {
    
    static let shared = AccessToken()
    
    func save(token: String) {
        KeychainWrapper.standard.set(token, forKey: "KeychainItem_accessToken")
    }
    
    func get() -> String {
        return KeychainWrapper.standard.string(forKey: "KeychainItem_accessToken") ?? ""
    }
    
}
