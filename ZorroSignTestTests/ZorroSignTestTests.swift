//
//  ZorroSignTestTests.swift
//  ZorroSignTestTests
//
//  Created by Akila Upendra on 12/17/19.
//  Copyright © 2019 Akila Upendra. All rights reserved.
//

import XCTest
@testable import ZorroSignTest

class ZorroSignTestTests: XCTestCase {

    func testLoginApi() {
        let loginVc = LoginViewController()
        
        XCTAssertTrue(loginVc.validCredentials(username: "Test", password: "Test"))
    }

}
